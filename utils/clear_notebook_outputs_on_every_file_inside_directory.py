# Assumes this script is run from the directory containing the notebooks

import os
import nbformat

def clear_outputs(notebook_path: str):
    """Clear outputs in a Jupyter notebook."""
    try:
        with open(notebook_path, 'r', encoding='utf-8') as file:
            nb = nbformat.read(file, as_version=4)
        for cell in nb.cells:
            if cell.cell_type == 'code':
                cell.outputs = []
                cell.execution_count = None
        with open(notebook_path, 'w', encoding='utf-8') as file:
            nbformat.write(nb, file)
        print(f"Outputs cleared for {notebook_path}")
    except Exception as e:
        print(f"Error clearing {notebook_path}: {str(e)}")

def find_and_clear_notebooks(start_directory):
    """Find and clear outputs of all notebooks in directory and subdirectories."""
    print(f"Starting in directory: {start_directory}")
    for dirpath, dirnames, filenames in os.walk(start_directory):
        for filename in filenames:
            if filename.endswith('.ipynb'):
                notebook_path = os.path.join(dirpath, filename)
                print(f"Clearing outputs for {notebook_path}")
                clear_outputs(notebook_path)

if __name__ == "__main__":
    start_directory = os.getcwd()
    find_and_clear_notebooks(start_directory)